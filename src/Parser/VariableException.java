package Parser;

public class VariableException extends Exception{

    VariableException(String message) {
        super(message);
    }
}
