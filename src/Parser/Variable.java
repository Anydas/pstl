package Parser;

import java.util.ArrayList;

public class Variable {
    private final String   name;
    private final String[] args;
    public  final String[] values;
    public  final String[] minValues;
    public  final String[] maxValues;

    Variable(final String name) {
        if (name == null || name.length() == 0)
            throw new RuntimeException("Name of the Variable is null or empty");

        this.name = name;
        this.args = null;
        this.values = null;
        this.minValues = null;
        this.maxValues = null;
    }

    Variable(final String name, String[] args, String[] values) {
        if (name == null || name.length() == 0)
            throw new RuntimeException("Name of the Variable is null or empty");

        if (values != null && args != null && values.length != args.length)
            throw new RuntimeException("Args name and values have different size");

        this.name = name;
        this.args = args;
        this.values = values;

        if (values != null) {
            this.minValues = new String[values.length];
            this.maxValues = new String[values.length];
            for (int i=0; i<values.length; i++) {
                this.minValues[i] = "0";
                this.maxValues[i] = this.values[i];
            }

        }
        else {
            this.minValues = null;
            this.maxValues = null;
        }
    }

    Variable(final String name, ArrayList<String> args, ArrayList<String> values) {
        if (name == null || name.length() == 0)
        throw new RuntimeException("Name of the Variable is null or empty");

        if (values != null && args != null && values.size() != args.size())
            throw new RuntimeException("Args name and values have different size");


        this.name = name;
        this.args = new String[args.size()];
        this.values = new String[values.size()];

        int i = 0;
        for (String arg : args) {
            this.args[i] = arg;
            i++;
        }

        i=0;
        for (String value : values) {
            this.values[i] = value;
            i++;
        }

        if (values != null) {
            this.minValues = new String[values.size()];
            this.maxValues = new String[values.size()];
            i=0;
            for (String s : values) {
                this.minValues[i] = "0";
                this.maxValues[i] = s;
                i++;
            }
        }
        else {
            this.minValues = null;
            this.maxValues = null;
        }
    }

    public String getName() {
        return name;
    }

    public String[] getArgs() {
        return args;
    }

    public String[] getValues() {
        return values;
    }

    public void setValues(String[] values) {
        if (values.length != this.values.length)
            throw new RuntimeException("Invalid values size (current : "+this.values.length+", new : "+values.length+")");
        System.arraycopy(values, 0, this.values, 0, values.length);
    }

    public void setValue(int index, String value) {
        this.values[index] = value;
    }

    public void setValue(String arg, String value) throws ArgNotFoundException {
        for (int i=0; i<args.length; i++) {
            if (args[i].equals(arg)) {
                setValue(i, value);
                return;
            }
        }
        throw new ArgNotFoundException(arg,name);
    }

    public void setMinValue(int index, String value) {
        this.minValues[index] = value;
    }

    public void setMinValue(String arg, String value) throws ArgNotFoundException {
        for (int i=0; i<args.length; i++) {
            if (args[i].equals(arg)) {
                setMinValue(i,value);
                return;
            }
        }
        throw new ArgNotFoundException(arg,name);
    }

    public void setMaxValue(int index, String value) {
        this.maxValues[index] = value;
    }

    public void setMaxValue(String arg, String value) throws ArgNotFoundException {
        for (int i=0; i<args.length; i++) {
            if (args[i].equals(arg)) {
                setMaxValue(i,value);
                return;
            }
        }
        throw new ArgNotFoundException(arg,name);
    }

    public boolean contains(String arg) {
        if (args == null || arg == null)
            return false;
        for (String arg1 : args) {
            if (arg1.equals(arg)) {
                return true;
            }
        }
        return false;
    }

    public String getMinValues(int id) {
        return minValues[id];
    }

    public String getMaxValues(int id) {
        return maxValues[id];
    }

    public String getValues(int id) {
        return values[id];
    }

}
