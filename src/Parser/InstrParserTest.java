package Parser;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

public class InstrParserTest {

	@BeforeClass
	public static void setup() throws VariableAlreadyExistsException {
        InstrParser.addInstrument("bass");

        String[] beatArgs = {"start","volume"};
        String[] beatValues = {"0","1"};

        InstrParser.addSound("beat",beatArgs,beatValues);
	}

    @Test
    public void testReturns() throws InvalidCommandException, VariableException {
        assertEquals("test\nof\na\nline\nsecond\n", InstrParser.parse("\n\n%%\ntest\nof\na\nline\n%%\n%%\nsecond\n%%"));
    }

    @Test
	public void testCodes() throws InvalidCommandException, VariableException {
		assertEquals("test\nof\na\nline\nsecond\n", InstrParser.parse("%%\ntest\nof\na\nline\n%%\n%%\nsecond\n%%"));
	}
	
	@Test
	public void testBeat() throws InvalidCommandException, VariableException {
		assertEquals("~x2 = Synth.new(\\beat, [\\start,2]);", InstrParser.parse("beat x2 start 2"));
	}


    @Test
    public void testAddSound() throws VariableAlreadyExistsException {
        InstrParser.addSound("pump");
    }

    @Test(expected = VariableAlreadyExistsException.class)
    public void testAddSoundError() throws VariableAlreadyExistsException {
        InstrParser.addSound("hit");

        // Should throw the Exception
        InstrParser.addSound("hit");}

    @Test
    public void testGetVarType() throws VariableException, InvalidCommandException {
        assertEquals("~xD = Synth.new(\\beat, [\\start,2]);", InstrParser.parse("beat xD start 2"));
        assertEquals("beat",InstrParser.getVarType("~xD"));
    }

    @Test(expected = VariableNotFoundException.class)
    public void testGetVarTypeError() throws VariableException, InvalidCommandException {
        // Should throw the Exception
        InstrParser.getVariable("aVarThatDoesntExist");}

    @Test
    public void testSetValueForVariable() throws VariableException, InvalidCommandException {
        assertEquals("~varValChange = Synth.new(\\beat, [\\start,2]);", InstrParser.parse("beat varValChange start 2"));
        InstrParser.setValueForVariable("~varValChange","start","4");
        Variable v = InstrParser.getVariable("~varValChange");
        for (int i=0; i<v.getArgs().length; i++)
            if (v.getArgs()[i].equals("start") && !v.getValues()[i].equals("4"))
                fail("The value hasn't change");

        String firstValue = v.getArgs()[0];

        v.setValue(0,"10");
        if (!v.getValues()[0].equals("10"))
            fail("The value hasn't change");

        v.setValue(firstValue,"20");
        if (!v.getValues()[0].equals("20"))
            fail("The value hasn't change");

        v.setMaxValue(0,"1000");
        if (v.maxValues != null && !v.maxValues[0].equals("1000"))
            fail("The maxValue hasn't change");

        v.setMaxValue(firstValue,"3000");
        if (v.maxValues != null && !v.maxValues[0].equals("3000"))
            fail("The maxValue hasn't change");

        v.setMinValue(0,"15");
        if (v.minValues != null && !v.minValues[0].equals("15"))
            fail("The minValue hasn't change");

        v.setMinValue(firstValue,"20");
        if (v.minValues != null && !v.minValues[0].equals("20"))
            fail("The minValue hasn't change");
    }




    @Test
    public void testGetSoundsVariables() throws VariableException, InvalidCommandException {
        assertEquals("~x42 = Synth.new(\\beat, [\\start,2]);", InstrParser.parse("beat x42 start 2"));
        assertTrue(InstrParser.getSoundsVariables().contains("~x42"));
    }
	
	@Test
	public void testComplex1() throws InvalidCommandException, VariableException {
		assertEquals("~x3 = Synth.new(\\beat);\nbeat y\n", InstrParser.parse("beat x3\n%%beat y\n%%"));
	}
	
	@Test
	public void testComplex2() throws InvalidCommandException, VariableException {
		assertEquals("test\nof\na\nline\n~x4 = Synth.new(\\beat, [\\volume,2]);\nsecond\n", InstrParser.parse("%%\ntest\nof\na\nline\n%%\nbeat x4 volume 2\n%%\nsecond\n%%"));
	}

	@Test(expected = InvalidInstructionException.class)
	public void testNotInstrument() throws InvalidCommandException, VariableException {
		// Should throw the Exception
		System.out.println(InstrParser.parse("unexist 1 2"));}
	
	@Test
	public void testChange() throws InvalidCommandException, VariableException {
		assertEquals("~a_var = Synth.new(\\beat, [\\start,2]);", InstrParser.parse("beat a_var start 2"));
		assertEquals("~a_var.set(\\echoLength,0);", InstrParser.parse("change a_var echoLength 0"));
	}
	
	@Test(expected = InvalidVariableException.class)
	public void testChangeNotExist() throws InvalidCommandException, VariableException {
		// Should throw the Exception
		System.out.println(InstrParser.parse("change anyvar echoLength 0"));}
	
	@Test
	public void testStop() throws InvalidCommandException, VariableException {
		assertEquals("~a_var_to_stop = Synth.new(\\beat, [\\start,2]);", InstrParser.parse("beat a_var_to_stop start 2"));
		assertEquals("~a_var_to_stop.free;", InstrParser.parse("stop a_var_to_stop"));
	}
	
	@Test(expected = InvalidVariableException.class)
	public void testFreeTwice() throws InvalidCommandException, VariableException {
		assertEquals("~a_var_to_free = Synth.new(\\beat, [\\start,0.5]);", InstrParser.parse("beat a_var_to_free start 0.5"));
		assertEquals("~a_var_to_free.free;", InstrParser.parse("stop a_var_to_free"));
		
		// Should throw the Exception
		System.out.println(InstrParser.parse("stop a_var_to_free"));}

	@Test
	public void testPlay() throws InvalidCommandException, VariableException {
		assertEquals("~play_var = Buffer.read(s, \"C:/Windows/Media/Alarm01.wav\",bufnum:0);\n~play_var.play;", InstrParser.parse("play play_var C:\\Windows\\Media\\Alarm01.wav"));
		assertEquals("~play_var.free;", InstrParser.parse("stop play_var"));
	}

	@Test
    public void testParseRawCodeArgs() throws InvalidCommandException, VariableException {
        assertEquals("SynthDef.new(\\beaty, {\n" +
                "        arg echoLength= 0.1, volume = 0.75, test = 0.20, soundFreq=1, fund = 20, maxPartial=1, width=0.5, electronicEffect = 100, start = 0, canal=1;", InstrParser.parse("%%SynthDef.new(\\beaty, {\n" +
                "        arg echoLength= 0.1, volume = 0.75, test = 0.20, soundFreq=1, fund = 20, maxPartial=1, width=0.5, electronicEffect = 100, start = 0, canal=1;%%"));
        Variable v = InstrParser.getVariable("beaty");
        String[] valuesExpected = {"0.1","0.75","0.20","1","20","1","0.5","100","0","1"};
        String[] argNamesExpected = {"echoLength","volume","test","soundFreq","fund","maxPartial","width","electronicEffect","start","canal"};

        assertNotNull(v);
        assertNotNull(v.values);
        assertEquals(10, v.values.length);

        for (int i=0; i<10; i++) {
            assertEquals(valuesExpected[i], v.getValues()[i]);
            assertEquals(argNamesExpected[i], v.getArgs()[i]);
        }

    }

    @Test
    public void testParseArgs() throws VariableException {
	    InstrParser.parseArgs("NewInstr","Controls:\n" +
                "ControlName  P 0 echoLength control 0.10000000149012\n" +
                "ControlName  P 1 volume control 0.75\n" +
                "ControlName  P 2 test control 0.20000000298023\n" +
                "ControlName  P 3 soundFreq control 1\n" +
                "ControlName  P 4 fund control 20\n" +
                "ControlName  P 5 maxPartial control 1\n" +
                "ControlName  P 6 width control 0.5\n" +
                "ControlName  P 7 electronicEffect control 100\n" +
                "ControlName  P 8 start control 0\n" +
                "ControlName  P 9 canal control 0\n" +
                "   O audio Out canal 1");

	    Variable instr = InstrParser.getVariable("NewInstr");
	    String[] args = {"echoLength","volume","test","soundFreq","fund","maxPartial","width","electronicEffect","start","canal"};
        String[] values = {"0.10000000149012","0.75","0.20000000298023","1","20","1","0.5","100","0","0"};

        assertEquals(args.length,instr.getArgs().length);
        assertEquals(values.length,instr.getValues().length);

        for (int i=0; i<args.length; i++) {
            assertEquals(args[i],instr.getArgs()[i]);
            assertEquals(values[i],instr.getValues()[i]);
        }

    }

	@Test
	public void testParseMidi() throws InvalidCommandException, VariableException {
		Object[] args = {1.3,0.5,50,"bass"};

		assertEquals("( ~x = Pseq([\n" +
                "    Pmono(\\bass,\n" +
                "        \\midinote, Pseq([50.0], 1),\n" +
                "        \\dur, Pseq([1.3], 1),\n" +
                "        \\amp, 0.5, \\detune, 1.005\n" +
                "    )], inf).play(quant: 1);\n" +
                ")", InstrParser.parseMIDI(args));
	}

}
