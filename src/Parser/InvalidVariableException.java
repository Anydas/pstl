package Parser;

public class InvalidVariableException extends InvalidCommandException {

	private static final long serialVersionUID = -4815429304599323032L;

	InvalidVariableException(String string) {
		super(string);
	}

	InvalidVariableException(String string, String usage) {
		super(string, usage);
	}
}
