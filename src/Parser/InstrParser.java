package Parser;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public abstract class InstrParser {
    public static final String SUPERCOLLIDER_PATH = "C:\\SuperCollider";

    private static int bufnum = 0;
    private static HashMap<String, Variable> variables = new HashMap<>();
    private static HashSet<String> instruments = new HashSet<>();
    private static HashSet<String> sounds = new HashSet<>();
    private static HashMap<String, String> varToType = new HashMap<>();
    private static HashSet<String> varsSounds = new HashSet<>();
    private static HashSet<String> varsIntruments = new HashSet<>();
    private static HashSet<String> varsFree = new HashSet<>();

    private static int currentNote = 0;

    private enum Type {
        INSTRUMENT, SOUND, PLAY, CHANGE, STOP
    }

    public static String parseMIDI(Object[] args) throws InvalidCommandException, VariableException {
        if (args == null)
            throw new InvalidArgsException("Cannot play the MIDI sound, args is null");
        if (args.length != 4)
            throw new InvalidArgsException("Cannot play the MIDI sound, args length is not equal to 4");

        double duree;
        double volume;
        double frequence;
        String instrument;

        if (!isNumber(String.valueOf(args[0]))) {
            throw new InvalidArgsException("Cannot play the MIDI sound, length is not a double value");
        }
        else
            duree = Double.parseDouble(String.valueOf(args[0]));

        if (!isNumber(String.valueOf(args[1]))) {
            throw new InvalidArgsException("Cannot play the MIDI sound, volume is not a double value");
        }
        else
            volume = Double.parseDouble(String.valueOf(args[1]));

        if (!isInteger(String.valueOf(args[2]))) {
            throw new InvalidArgsException("Cannot play the MIDI sound, frequency is not a double value");
        }
        else
            frequence = Integer.parseInt(String.valueOf(args[2]));

        if (!instruments.contains(String.valueOf(args[3])))
            throw new InvalidArgsException("Cannot play the MIDI sound, instrument '"+String.valueOf(args[3])+"' is not defined");
        else
            instrument = String.valueOf(args[3]);

        currentNote++;
        return parse(instrument + " _x" + currentNote + " " + frequence + " " + duree + " " + volume);
    }

    public static String parse(String lines) throws InvalidCommandException, VariableException {
        if (lines == null || lines.length() == 0)
            return lines;

        if (lines.startsWith("\n")) {
            lines = lines.substring(1);
            if (lines.length() == 0)
                return lines;
        }

        if (lines.startsWith("%%")) {
            return parseCode(lines);
        }

        String[] linesArray = lines.split("\n");
        StringBuilder sb = new StringBuilder();
        int nbchar = 0;
        for (String s : linesArray) {
            if (s.length() == 0)
                continue;
            if (s.startsWith("%%")) {
                sb.append(parse(lines.substring(nbchar)));
                break;
            }
            nbchar += s.length();
            sb.append(parseLine(s));
            if (linesArray.length > 1 || linesArray[0].endsWith("\n"))
                sb.append("\n");
        }
        return sb.toString();
    }

    private static String parseLine(String line) throws InvalidCommandException, VariableException {
        if (line == null || line.length() == 0)
            return line;

        if (line.startsWith("%%")) {
            return parseCode(line);
        }

        Type t = null;

        if (line.startsWith("change"))
            t = Type.CHANGE;

        else if (line.startsWith("stop"))
            t = Type.STOP;

        else if (line.startsWith("play"))
            t = Type.PLAY;

        else {
            for (String instr : instruments) {
                if (line.startsWith(instr)) {
                    t = Type.INSTRUMENT;
                    break;
                }
            }
            for (String instr : sounds) {
                if (line.startsWith(instr)) {
                    t = Type.SOUND;
                    break;
                }
            }
        }

        if (t == null) {
            if (line.split(" ").length == 0)
                throw new InvalidInstructionException("Invalid instrument or sound : '" + line + "'", "beat var");
            else
                throw new InvalidInstructionException("Invalid instrument or sound : '" + line.split(" ")[0] + "'", "beat var");
        }

        final String[] args = line.split(" ");
        StringBuilder sb = new StringBuilder();
        switch (t) {
            case INSTRUMENT:
                if (args.length < 2)
                    throw new InvalidInstructionException("You must specify the variable, the note and its length to be played", "bass x 45,50 1.1,0.5");
                if (args.length < 4)
                    throw new InvalidInstructionException("You must specify the variable, the note and its length to be played", args[0] + " " + args[1] + " 45,50 1.1,0.5");
                args[1] = checkVarNameString(args[1]);

                String volume = "0.5";
                if (args.length >= 5) {
                    if (!isNumber(args[4]))
                        throw new InvalidArgsException("The value '" + args[4] + "' is not a number", args[0] + " " + args[1] + " " + args[2] + " " + args[3] + " 0.5");
                    else
                        volume = args[4];
                }

                sb.append("( ").append(args[1]).append(" = Pseq([\n")
                        .append("    Pmono(\\").append(args[0]).append(",\n")
                        .append("        \\midinote, Pseq([").append(args[2]).append("], 1),\n")
                        .append("        \\dur, Pseq([").append(args[3]).append("], 1),\n")
                        .append("        \\amp, ").append(volume).append(", \\detune, 1.005\n")
                        .append("    )], inf).play(quant: 1);\n")
                        .append(")");

                varsIntruments.add(args[1]);
                varsFree.remove(args[1]);
                break;
            case PLAY:
                if (args.length < 3)
                    throw new InvalidInstructionException("You must specify a variable name for the sound and the path to the file", "play x mysound.wav");
                args[1] = checkVarNameString(args[1]);
                File f;

                args[2] = args[2].replace("\\", "/");

                // Check absolute path
                if ((args[2].startsWith("/")) || (args[2].length() >= 2 && args[2].charAt(1) == ':')) {
                    f = new File(args[2]);
                    args[2] = "\"" + args[2] + "\"";
                } else {
                    f = new File(SUPERCOLLIDER_PATH, args[2]);
                    args[2] = "Platform.resourceDir +/+ \"sounds/" + args[2] + "\"";
                }

                if (!f.exists())
                    throw new InvalidInstructionException("The file '" + f.getPath() + "' does not exist");
                if (!f.canRead())
                    throw new InvalidInstructionException("Cannot read the file '" + f.getPath() + "'");

                sb.append(args[1]).append(" = Buffer.read(s, ").append(args[2]).append(",bufnum:").append(bufnum).append(");\n")
                        .append(args[1]).append(".play;");

                bufnum++;
                varsSounds.add(args[1]);
                varsFree.remove(args[1]);
                break;
            case STOP:
                if (args.length < 2)
                    throw new InvalidInstructionException("You must specify the variable to stop", "stop x");
                if ("".equals(args[1]) || args[1] == null)
                    throw new EmptyStringException("Empty string is not allowed as a variable name");
                if (!args[1].startsWith("~"))
                    args[1] = "~" + args[1];
                if (varsFree.contains(args[1]))
                    throw new InvalidVariableException("Variable " + args[1] + " has already been freed");
                if (!varsSounds.contains(args[1]) && !varsIntruments.contains(args[1]))
                    throw new InvalidVariableException("Variable " + args[1] + " does not exist");
                if (varsSounds.contains(args[1])) {
                    varsSounds.remove(args[1]);
                    sb.append(args[1]).append(".free;");
                } else {
                    varsIntruments.remove(args[1]);
                    sb.append(args[1]).append(".stop;");
                }
                variables.remove(args[1]);
                varsFree.add(args[1]);
                break;
            case SOUND:
                if (args.length == 1)
                    throw new InvalidInstructionException("You must specify a variable name for the sound", args[0] + " x");
                if (args.length < 2)
                    throw new InvalidInstructionException("You must specify a variable name for the sound", "beat x");

                args[1] = checkVarNameString(args[1]);
                sb.append(args[1]).append(" = ");
                varsSounds.add(args[1]);
                varsFree.remove(args[1]);
                sb.append("Synth.new(\\");
                sb.append(args[0]);
                if (args.length > 2) {
                    boolean isName = true;

                    sb.append(", [");
                    for (int i = 2; i < args.length; i++) {
                        if (isNumber(args[i])) {
                            if (isName) {
                                if (i == 2)
                                    throw new InvalidArgsException("The value '" + args[i] + "' has no argument name");
                                else
                                    throw new InvalidArgsException("The value '" + args[i - 1] + "' has no argument name");
                            }
                            isName = true;
                        } else {
                            if (!isName) {
                                throw new InvalidArgsException("The argument '" + args[i - 1] + "' has no value");
                            }
                            sb.append("\\");
                            isName = false;
                        }
                        sb.append(args[i]);
                        sb.append(",");
                    }
                    if (args.length % 2 == 1) {
                        throw new InvalidArgsException("The argument '" + args[args.length - 1] + "' has no value");
                    }
                    sb.deleteCharAt(sb.length() - 1);
                    sb.append("]");
                }
                sb.append(");");

                Variable orig = getVariable(args[0]);
                for (int i = 2; i < args.length; i += 2)
                    if (!orig.contains(args[i]))
                        throw new ArgNotFoundException(args[i], args[0]);

                variables.put(args[1], new Variable(args[1], orig.getArgs(), orig.getValues()));
                Variable newVar = variables.get(args[1]);
                for (int i = 2; i < args.length; i += 2) {
                    newVar.setValue(args[i], args[i + 1]);
                }

                varToType.put(args[1], args[0]);
                break;
            case CHANGE:
                boolean asTild = false;
                if (args.length <= 2)
                    throw new InvalidInstructionException("You must specify the name of the variable, the arg(s) and their value to change", "change x volume 0.5");
                if (args[1] != null)
                    asTild = args[1].startsWith("~");
                if ("".equals(args[1]) || args[1] == null)
                    throw new EmptyStringException("Empty string is not allowed as a variable name");
                if (!args[1].startsWith("~"))
                    args[1] = "~" + args[1];
                if (varsIntruments.contains(args[1]))
                    throw new InvalidVariableException("Variable " + args[1] + " is an Instrument");
                if (!varsSounds.contains(args[1]))
                    throw new InvalidVariableException("Variable " + args[1] + " does not exist");

                sb.append(args[1]);
                sb.append(".set(");

                if (args.length > 2) {
                    boolean isName = true;
                    for (int i = 2; i < args.length; i++) {
                        if (isNumber(args[i])) {
                            if (isName) {
                                if (i == 3)
                                    throw new InvalidArgsException("The value '" + args[i] + "' has no argument name");
                                else
                                    throw new InvalidArgsException("The value '" + args[i - 1] + "' has no argument name");
                            }
                            isName = true;
                        } else {
                            if (!isName) {
                                throw new InvalidArgsException("The argument '" + args[i - 1] + "' has no value");
                            }
                            sb.append("\\");
                            isName = false;
                        }
                        sb.append(args[i]);
                        sb.append(",");
                    }
                    if (args.length % 2 == 1) {
                        throw new InvalidArgsException("The argument '" + args[args.length - 1] + "' has no value");
                    }
                    sb.deleteCharAt(sb.length() - 1);
                } else {
                    if (!asTild)
                        args[1] = args[1].substring(1);
                    throw new InvalidInstructionException("You must specify the arg(s) and their value to change", "change " + args[1] + " volume 0.5");
                }
                sb.append(");");
                break;
        }

        return sb.toString();
    }

    private static String codeLine(String code) throws VariableAlreadyExistsException {
        int indexStartSound, indexEndSound;
        int indexStartArg, indexEndArg;
        boolean isInst = false;

        // Contains instr or sound
        if ((indexStartSound = code.indexOf("SynthDef")) != -1) {

            if (code.substring(indexStartSound,indexStartSound+50).contains("|"))
                isInst = true;
            else
                isInst = false;

            while (code.charAt(indexStartSound) != '\\') {
                if (indexStartSound == code.length() - 1)
                    return code;
                indexStartSound++;
            }
            if (++indexStartSound >= code.length())
                return code;

            indexEndSound = indexStartSound;

            while (code.charAt(indexEndSound) != ',') {
                if (indexEndSound == code.length() - 1)
                    return code;
                indexEndSound++;
            }


            String name = code.substring(indexStartSound, indexEndSound);
            if (code.contains("|"))
                addInstrument(name);

            if ((indexStartArg = code.indexOf("arg")) == -1) {
                if (isInst)
                    instruments.add(name);
                else {
                    sounds.add(name);
                    addVariable(new Variable(name));
                }
                return code;
            }

            ArrayList<String> args = new ArrayList<>();
            ArrayList<String> values = new ArrayList<>();
            indexStartArg += 4;

            while (indexStartArg < code.length() && code.charAt(indexStartArg) != ';') {
                // Arg name
                indexEndArg = indexStartArg;

                while (code.charAt(indexEndArg) != '=' && code.charAt(indexEndArg) != ';') {
                    if (indexEndArg == code.length() - 1)
                        return code;
                    indexEndArg++;
                }
                if (code.charAt(indexEndArg) == ';')
                    break;

                args.add(code.substring(indexStartArg, indexEndArg).trim());

                //Arg value
                if ((indexStartArg = indexEndArg + 1) >= code.length()) {
                    args.remove(args.size() - 1);
                    break;
                }

                indexEndArg = indexStartArg;

                while (code.charAt(indexEndArg) != ',' && code.charAt(indexEndArg) != ';') {
                    if (indexEndArg == code.length() - 1)
                        return code;
                    indexEndArg++;
                }

                if (code.charAt(indexEndArg) == ';' && indexEndArg == indexStartArg) {
                    args.remove(args.size() - 1);
                    break;
                }

                values.add(code.substring(indexStartArg, indexEndArg).trim());
                indexStartArg = indexEndArg + 2;
            }
            if (isInst)
                instruments.add(name);
            else {
                sounds.add(name);
                addVariable(new Variable(name, args, values));
            }
        }
        return code;
    }

    private static String parseCode(String lines) throws InvalidCommandException, VariableException {
        String code;
        int decal = 0;
        if (lines.startsWith("%%\n")) {
            decal += 3;
            code = lines.substring(3);
        } else {
            decal += 2;
            code = lines.substring(2);
        }
        if (code.contains("%%")) {
            final String trueCode = code.substring(0, code.indexOf("%%"));
            if (trueCode.length() >= trueCode.indexOf("%%") + 2
                    && trueCode.charAt(trueCode.indexOf("%%") + 2) == '\n')
                return codeLine(trueCode) + parse(lines.substring(trueCode.length() + decal + 3));
            else
                return codeLine(trueCode) + parse(lines.substring(trueCode.length() + decal + 2));
        } else {
            throw new InvalidInstructionException("Could not find the end of the code, '%%' missing in :\n" + lines, "%%~x = Synth.new(\\beat, [\\start, 0, \\canal, 0]);%%");
        }
    }

    public static void parseArgs(final String name, final String line) throws VariableAlreadyExistsException {
        if (variables.containsKey(name))
            throw new VariableAlreadyExistsException(name);

        final String[] lines = line.split("\n");
        final ArrayList<String> args = new ArrayList<>();
        final ArrayList<String> values = new ArrayList<>();

        String[] lineWords;
        for (String aline : lines) {
            if (aline.startsWith("ControlName")) {
                //ControlName  P 0 echoLength control 0.10000000149012
                aline = aline.substring(13);
                lineWords = aline.split(" ");
                args.add(lineWords[2]);
                values.add(lineWords[4]);
            }

        }

        addVariable(new Variable(name, args, values));
    }

    private static void addVariable(Variable variable) throws VariableAlreadyExistsException {
        if (variables.containsKey(variable.getName()))
            throw new VariableAlreadyExistsException(variable.getName());
        variables.put(variable.getName(), variable);
    }

    public static Variable getVariable(String varName) throws VariableNotFoundException {
        Variable v;
        if ((v = variables.get(varName)) == null)
            throw new VariableNotFoundException(varName);
        return v;
    }

    public static HashSet<String> getSoundsVariables() {
        return varsSounds;
    }

    public static void addInstrument(String instrument) {
        instruments.add(instrument);
    }

    public static void addSound(String name) throws VariableAlreadyExistsException {
        addVariable(new Variable(name));
        sounds.add(name);
    }

    public static void addSound(String name, String[] args, String[] values) throws VariableAlreadyExistsException {
        addVariable(new Variable(name, args, values));
        sounds.add(name);
    }

    public static String getVarType(String varName) throws VariableNotFoundException {
        if (!varToType.containsKey(varName))
            throw new VariableNotFoundException(varName);
        return varToType.get(varName);
    }

    public static boolean varExists(String varName) {
        return variables.containsKey(varName);
    }

    public static void setValueForVariable(final String name, final String arg, final String value) throws VariableException {
        Variable v;
        if ((v = variables.get(name)) == null)
            throw new VariableNotFoundException(name);
        v.setValue(arg, value);
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private static boolean isNumber(String s) {
        try {
            Double.parseDouble(s);
            return true;
        } catch (Exception ignored) {
            return false;
        }
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
            return true;
        } catch (Exception ignored) {
            return false;
        }
    }

    private static String checkVarNameString(String s) throws InvalidCommandException {
        if ("".equals(s) || s == null)
            throw new EmptyStringException("Empty string is not allowed as a variable name");
        if (!s.startsWith("~"))
            s = "~" + s;
        for (String instr : varsIntruments) {
            if (s.equals(instr))
                throw new InvalidVariableException("Variable " + s + " already exists as an Instrument");
        }
        for (String sound : varsSounds) {
            if (s.equals(sound))
                throw new InvalidVariableException("Variable " + s + " already exists as a Sound");
        }
        return s;
    }
}