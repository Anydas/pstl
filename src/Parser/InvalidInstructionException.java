package Parser;

public class InvalidInstructionException extends InvalidCommandException {

	private static final long serialVersionUID = 4484428594994343627L;

	InvalidInstructionException(String string) {
		super(string);
	}

	InvalidInstructionException(String string, String usage) {
		super(string, usage);
	}
}
