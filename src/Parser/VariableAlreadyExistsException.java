package Parser;

public class VariableAlreadyExistsException extends VariableException{

    VariableAlreadyExistsException(String name) {
        super("The variable "+name+" already exists");
    }
}
