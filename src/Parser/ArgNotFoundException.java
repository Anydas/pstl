package Parser;

public class ArgNotFoundException extends VariableException {

	private static final long serialVersionUID = -6432957045245168024L;

	ArgNotFoundException(String argName, String soundName) {
		super("The argument '"+argName+"' isn't an argument of '"+soundName+"'");
	}

}
