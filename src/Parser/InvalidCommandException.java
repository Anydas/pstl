package Parser;

public class InvalidCommandException extends Exception {

    private static final long serialVersionUID = -2986912902923401452L;
    
    InvalidCommandException(String string) {
        super(string);
    }

    InvalidCommandException(String string, String usage) {
        super(string+"\n\tUsage : "+usage);
    }

}