package Parser;

public class EmptyStringException extends InvalidCommandException {

	private static final long serialVersionUID = 2127660120002810834L;

	EmptyStringException(String string) {
		super(string);
	}

	EmptyStringException(String string, String usage) {
		super(string,usage);
	}
}
