package Parser;

public class InvalidArgsException extends InvalidCommandException {

	private static final long serialVersionUID = -8527797045613768005L;

	InvalidArgsException(String string) {
		super(string);
	}

	InvalidArgsException(String string, String usage) {
		super(string,usage);
	}
}
