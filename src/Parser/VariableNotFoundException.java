package Parser;

public class VariableNotFoundException extends VariableException{

    VariableNotFoundException(String name) {
        super("The variable "+name+" does not exist");
    }
}