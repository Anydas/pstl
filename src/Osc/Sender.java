package Osc;

import com.illposed.osc.OSCMessage;
import com.illposed.osc.OSCPortOut;

import java.net.InetAddress;

/**
 * Created by Valentin on 19/02/2017.
 */
public class Sender {

    public static void sendOsc(Object[] args) throws Exception {
        InetAddress remoteIP = InetAddress.getLocalHost();
        OSCPortOut sender = new OSCPortOut(remoteIP, 57120);
        OSCMessage msg = new OSCMessage("/chat", args);

        try {

            sender.send(msg);


        } catch (Exception e) {
            System.out.println("pb osc");
        }
    }

    public static void sendOsc(Object[] args, String name) throws Exception {
        InetAddress remoteIP = InetAddress.getLocalHost();
        OSCPortOut sender = new OSCPortOut(remoteIP, 57120);
        if (!name.startsWith("/")) {
            name = "/" + name;
        }
        OSCMessage msg = new OSCMessage(name, args);

        try {

            sender.send(msg);


        } catch (Exception e) {
            System.out.println("pb osc");
        }
    }
}
