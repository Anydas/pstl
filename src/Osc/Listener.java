package Osc;

import Controller.TestController;
import Parser.InstrParser;
import Parser.InvalidCommandException;
import Parser.VariableAlreadyExistsException;
import Parser.VariableException;
import com.illposed.osc.OSCListener;
import com.illposed.osc.OSCMessage;
import com.illposed.osc.OSCPort;
import com.illposed.osc.OSCPortIn;

import java.net.SocketException;

/**
 * Created by val41 on 05/03/2017.
 */

public class Listener {
    private static OSCPortIn receiver = null;

    public static void listen(String name) throws SocketException {
        receiver = new OSCPortIn(50);
        OSCListener listener = new OSCListener() {
            public void acceptMessage(java.util.Date time, OSCMessage message) {
                System.out.println("Message received!");

                Object[] o = message.getArguments();

                try {
                    System.out.println(String.valueOf(o[0]));
                    InstrParser.parseArgs(name, String.valueOf(o[0]));
                } catch (VariableAlreadyExistsException e) {
                    e.printStackTrace();
                }

                if (!String.valueOf(o[0]).toLowerCase().equals("nil"))
                    TestController.setSynchro(true);

                receiver.stopListening();
                System.out.println(receiver.isListening());
                receiver.close();

            }
        };
        receiver.addListener("/resp", listener);

        receiver.startListening();

    }

    public static void listen() throws SocketException {
        receiver = new OSCPortIn(50);
        OSCListener listener = new OSCListener() {
            public void acceptMessage(java.util.Date time, OSCMessage message) {
                System.out.println("Message received!");

                Object[] o = message.getArguments();

                if (!String.valueOf(o[0]).toLowerCase().equals("nil"))
                    try {
                        InstrParser.parse(String.valueOf(o[0]));
                    } catch (InvalidCommandException e) {
                        e.printStackTrace();
                    } catch (VariableException e) {
                        e.printStackTrace();
                    }

                receiver.stopListening();
                System.out.println(receiver.isListening());
                receiver.close();

            }
        };
        receiver.addListener("/resp", listener);

        receiver.startListening();


    }

    public static void listenResp() throws SocketException {
        receiver = new OSCPortIn(50);
        OSCListener listener = new OSCListener() {
            public void acceptMessage(java.util.Date time, OSCMessage message) {
                System.out.println("Message received!");
                Object[] o = message.getArguments();
                if (String.valueOf(o[0]).toLowerCase().equals("OK")){
                    TestController.setSynchro(true);
                    receiver.stopListening();
                    System.out.println(receiver.isListening());
                    receiver.close();
                }
            }
        };
        receiver.addListener("/resp", listener);
        receiver.startListening();

    }

    public static void close() {
        receiver.stopListening();
        receiver.close();
    }
}
