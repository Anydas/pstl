package Controller;

import Osc.Listener;
import Parser.InstrParser;
import Parser.InvalidCommandException;
import Parser.Variable;
import Parser.VariableAlreadyExistsException;
import Parser.VariableNotFoundException;
import Parser.VariableException;
import Parser.ArgNotFoundException;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;

import java.io.*;
import java.net.SocketException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import javafx.scene.layout.*;
import javafx.stage.FileChooser;
import javafx.stage.Screen;
import javafx.stage.Stage;

import javax.swing.text.html.parser.Parser;

/**
 * Created by val41 on 12/02/2017.
 */
public class TestController {

    public static final boolean DEBUG = false;

    // Reference to the main application.
    @FXML
    private TextArea live;

    @FXML
    private TextArea console;

    @FXML
    private TextArea print;

    @FXML
    private MenuBar bar;

    @FXML
    private ChoiceBox boxx;

    /*@FXML
    private AnchorPane cadre;*/

    @FXML
    private ScrollPane Scroll;

    private Main mainApp;

    private String varUtil;

    private boolean listener = false;

    private static boolean synchro = false;

    //nouvelle ligne a send
    private ArrayList<String> send = new ArrayList<String>();

    //lignes modif
    private ArrayList<String> listMod = new ArrayList<String>();

    //ligne envoyé
    private ArrayList<String> listEnv = new ArrayList<String>();


    /**
     * The constructor.
     * The constructor is called before the initialize() method.
     */
    public TestController() {
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {

        loadAllLibrairies();

        /*try {
            Listener.listen();
        } catch (SocketException e) {
            //e.printStackTrace();
            console.setText(console.getText() + e + "\n");
        }*/


    }

    public void loadAllLibrairies() {
        String[] listefichiers;
        File f = new File("librairies");
        if(!f.exists() || !f.canRead()){
            if (DEBUG)
                System.out.println("nop");
            if (DEBUG)
                console.setText("Aucune librairie à charger\n");
            return;
        }
       // console.setText("deb charg" + "\n");
        listefichiers = f.list();
        for (int i = 0; i < listefichiers.length; i++) {
            System.out.println(listefichiers[i]);
            List<String> lines = new ArrayList<>();
            try {
                if (DEBUG)
                    System.out.println("avant");
                lines = Files.readAllLines(Paths.get("librairies/"+listefichiers[i]));
                if (DEBUG)
                    System.out.println("apres");
            } catch (IOException e) {
                //e.printStackTrace();
                console.setText(console.getText() + e + "\n");
            }
            StringBuilder sb = new StringBuilder("");
            for (String s : lines)
                sb.append(s);


            try {
                InstrParser.parse("%%"+sb.toString()+"%%");
                send(sb.toString());
            } catch (InvalidCommandException e) {
                //e.printStackTrace();
                console.setText(console.getText() + e + "\n");
            } catch (VariableException e) {
                //e.printStackTrace();
                console.setText(console.getText() + e + "\n");
            }
        }
        //console.setText("fin charg" + "\n");
        console.setText("Librairies chargées" + "\n");

    }

    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setMainApp(Main mainApp) {
        this.mainApp = mainApp;

        // Add observable list data to the table

    }

    private String getVar(String line) {
        String tab[] = line.split(" ");
        if (tab.length <= 1) {
            return "";
        }
        return tab[1];

    }

    private int occur(String line) {
        int nb = -1;
        char[] tab = line.toCharArray();
        for (int i = 0; i < tab.length; i++) {
            if (tab[i] == '\n') {
                nb++;
            }
        }
        return nb;
    }

    private ArrayList<String> reformateTab(String textarea) {
        ArrayList<String> ret = new ArrayList<>();
        String[] tab = textarea.split("\\$");

        for (int i = 0; i < tab.length; i++) {
            if (tab[i].contains("%%")) {
                System.out.println("code sc");
                ret.add(tab[i]);
                continue;
            }
            if (tab[i].equals("") || tab[i].equals(" ") || tab[i].equals("\n")) {
                ret.add("");
                System.out.println("ajout chaine vide");
                continue;
            }
            System.out.println("autres cas");
            int occ = occur(tab[i]);
            for (int j = 0; j < occ; j++)
                ret.add("");
            String r = tab[i].replace("\n", "");
            ret.add(r);

        }

        return ret;
    }


    public void testText() throws FileNotFoundException {
        print.setText("");

        // System.out.println("test");
        System.out.println("----------------------------");
        String s = live.getText();

        //si la text area est vide aucun traitement donc on sort
        if (s.equals("")) {
            System.out.println("sortie");
            return;
        }

        char lastchar = s.charAt(s.length() - 1);
        ArrayList<String> tab = reformateTab(s);
        if (lastchar == '\n') {
            // System.out.println(s);
            lastchar = '$';
        }

        if (lastchar == '$') {
            int compt = -1;
            for (String line : tab) {
                compt++;

                //if (!listEnv.contains(tab.get(i))) {
                boolean test = true;

                if (compt < listEnv.size()) {
                    test = (!listEnv.get(compt).equals(line));
                }
                if (test) {

                    if (listEnv.isEmpty()) {
                        System.out.println("premier ajout");
                        send.add(line);
                        listEnv.add(line);
                        continue;
                    }
                    if (compt > listEnv.size() - 1) {

                        System.out.println("ajout dans la liste");
                        send.add(line);
                        listEnv.add(line);
                        continue;
                    } else {
                        if (line.equals("")) {
                            System.out.println("Modit à vide");

                            if (!listEnv.get(compt).equals(""))
                                listMod.add("stop " + getVar(listEnv.get(compt)));

                            listEnv.remove(compt);
                            listEnv.add(compt, "");
                            continue;
                        } else {
                            System.out.println("Modif avec elem");

                            if (!listEnv.get(compt).equals("") && !listEnv.get(compt).startsWith("%%")){

                                listMod.add("stop " + getVar(listEnv.get(compt)));
                            }


                            listMod.add(line);
                            listEnv.remove(compt);
                            listEnv.add(compt, line);
                            continue;
                        }
                    }

                }

            }

            //print.setText(send.size()+ "\n");
            if (!send.isEmpty()) {
                String sendToSc = "";
                for (int i = 0; i < send.size(); i++) {
                    try {
                        System.out.println("------------------------------");
                        System.out.println(send.get(i));
                        System.out.println("------------------------------");
                        System.out.println();

                        sendToSc = InstrParser.parse(send.get(i));
                        //System.out.println(sendToSc);
                        print.setText(print.getText() + sendToSc + "\n");
                        //lines.add(sendToSc);
                        send(sendToSc);


                    } catch (InvalidCommandException invalidCommand) {
                        //invalidCommand.printStackTrace();
                        send.remove(send.get(i));
                        console.setText(console.getText() + invalidCommand + "\n");
                    } catch (VariableException e) {
                       // e.printStackTrace();
                        send.remove(send.get(i));
                        console.setText(console.getText() + e + "\n");
                    }
                    //sendToSc = "";
                }
            }
            print.setText(print.getText());//+send.size()+ "\n");
            if (!listMod.isEmpty()) {
                String mod = "";
                for (int i = 0; i < listMod.size(); i++) {
                    try {

                        mod = InstrParser.parse((String) listMod.get(i));
                        // System.out.println(mod);
                        print.setText(print.getText() + mod + "\n");
                        // lines.add(mod);
                        send(mod);

                    } catch (InvalidCommandException invalidCommand) {
                        //invalidCommand.printStackTrace();
                        listMod.remove(listMod.get(i));
                        console.setText(console.getText() + invalidCommand + "\n");
                    } catch (VariableException e) {
                        //e.printStackTrace();
                        listMod.remove(listMod.get(i));
                        console.setText(console.getText() + e + "\n");
                    }
                    //mod = "";
                }


            }


        }
        HashSet<String> varSounds = InstrParser.getSoundsVariables();
        boxx.setItems(FXCollections.observableArrayList(varSounds));
        send.clear();
        listMod.clear();
        //listEnv.remove("");
        //lines.clear();
        System.out.println("----------------------------");


    }

    private void send(String line) {
        try {
            //System.out.println(Files.deleteIfExists(Paths.get("test.sc")));
            //Files.write(Paths.get("test.sc"), lines, StandardCharsets.UTF_8);
            System.out.println("Send to server :"+line);
            Object[] arg = new Object[1];
            arg[0] = line;
            Osc.Sender.sendOsc(arg, "/code");


        } catch (IOException e) {
            //  e.printStackTrace();
            console.setText(console.getText() + e + "\n");
        } catch (Exception e) {
           // e.printStackTrace();
            console.setText(console.getText() + e + "\n");
        }

    }

    public void close() {
        mainApp.getPrimaryStage().close();
    }

    public void about() {

        final Label lab = new Label();
        lab.setText("Hugues LORIOT\nJérôme PHAM\nBoris LENOIR\nValentin BONNEAU");
        final Pane root = new Pane();
        /* //root.getChildren().add(lab);
        FlowPane tt =new FlowPane();
        tt.getChildren().add(new Slider());
        tt.getChildren().add(new Slider());
        tt.getChildren().add(new Slider());
        tt.setCenterShape(true);*/
        root.getChildren().addAll(lab);
        root.setCenterShape(true);

        final Scene scene = new Scene(root, 300, 100);
        Stage primaryStage = new Stage();
        primaryStage.setTitle("About");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public void changeValue(String id, String value) {

        try {
            print.setText("");
            System.out.println(value);
            InstrParser.setValueForVariable(varUtil, id, value);
            String line = InstrParser.parse("change " + varUtil + " " + id + " " + value);
            print.setText(print.getText() + line + "\n");
            send(line);

        } catch (InvalidCommandException invalidCommand) {
            //invalidCommand.printStackTrace();
            console.setText(console.getText() + invalidCommand + "\n");
        } catch (VariableException e) {
            //e.printStackTrace();
            console.setText(console.getText() + e + "\n");
        }
    }


    public void sauvVar() {
        setSynchro(false);
        System.out.println("Ok");


        if (!listener) {
            boxx.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
                @Override
                public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                    if (boxx.getValue() == null) {
                        varUtil = "";

                    } else {
                        varUtil = String.valueOf(boxx.getValue());


                        System.out.println("OK");
                        Variable v = null;
                        try {
                            v = InstrParser.getVariable(varUtil);
                        } catch (VariableNotFoundException e) {
                            //e.printStackTrace();
                            console.setText(console.getText() + e + "\n");
                        }


                        VBox test = new VBox();
                        int compteur = 0;
                        // cadre.getChildren().clear();
                        for (String s : v.getArgs()) {

                            final Slider tt = new Slider();
                            tt.setMin(Double.parseDouble(v.getMinValues(compteur)));
                            tt.setMax(Double.parseDouble(v.getMaxValues(compteur)));
                            tt.setValue(Double.parseDouble(v.getValues(compteur)));
                            tt.setMajorTickUnit(0.1);
                            tt.setMinorTickCount(0);
                            tt.setBlockIncrement((Double.parseDouble(v.getMaxValues(compteur)) + Double.parseDouble(v.getMinValues(compteur))) / 10);
                            tt.setShowTickLabels(true);
                            tt.setShowTickMarks(true);
                            tt.setId(s);
                            tt.valueProperty().addListener(new ChangeListener<Number>() {
                                public void changed(ObservableValue<? extends Number> ov,
                                                    Number old_val, Number new_val) {
                                    //demander si il reallou un nouvel dans les var a chaque fois que je lui envoie les args

                                    changeValue(tt.getId(), new_val.toString());

                                }
                            });
                            tt.setVisible(true);
                            Label l = new Label(s + " :");
                            test.getChildren().add(l);
                            //test.getChildren().add(tt);

                            FlowPane flowPane = new FlowPane();
                            flowPane.setPadding(new Insets(10, 10, 10, 10));
                            flowPane.setVgap(4);
                            flowPane.setHgap(4);
                            flowPane.setPrefWrapLength(525);
                            flowPane.getChildren().add(new Label("minValue :"));
                            TextField tfmin = new TextField();
                            tfmin.setId(s);
                            tfmin.textProperty().addListener((obs1, old1, new1) -> {
                                tt.setMin(Double.parseDouble(new1));
                                tt.setMajorTickUnit(0.1);
                                tt.setMinorTickCount(0);
                                tt.setBlockIncrement((tt.getMax() - tt.getMin()) / 10);
                                tfmin.setText(new1);
                                try {
                                    InstrParser.getVariable(varUtil).setMinValue(tfmin.getId(), new1);
                                } catch (ArgNotFoundException e) {
                                    console.setText(console.getText() + e + "\n");
                                   // e.printStackTrace();
                                } catch (VariableNotFoundException e) {
                                    console.setText(console.getText() + e + "\n");
                                    //e.printStackTrace();
                                }
                            });
                            tfmin.setText(v.getMinValues(compteur));
                            tfmin.setPrefWidth(30);
                            tfmin.setVisible(true);
                            flowPane.getChildren().add(tfmin);
                            flowPane.getChildren().add(tt);
                            flowPane.getChildren().add(new Label("maxValue"));
                            TextField tfmax = new TextField();
                            tfmax.setText(v.getMaxValues(compteur));
                            tfmax.setId(s);
                            tfmax.textProperty().addListener((obs2, oldV2, new2) -> {
                                tt.setMax(Double.parseDouble(new2));
                                tt.setMajorTickUnit(0.1);
                                tt.setMinorTickCount(0);
                                tt.setBlockIncrement((tt.getMax() - tt.getMin()) / 10);
                                tfmax.setText(new2);
                                try {
                                    InstrParser.getVariable(varUtil).setMaxValue(tfmax.getId(), new2);
                                } catch (ArgNotFoundException e) {
                                   // e.printStackTrace();
                                    console.setText(console.getText() + e + "\n");
                                } catch (VariableNotFoundException e) {
                                    //e.printStackTrace();
                                    console.setText(console.getText() + e + "\n");
                                }
                            });
                            tfmax.setPrefWidth(30);
                            tfmax.setVisible(true);
                            flowPane.getChildren().add(tfmax);
                            test.getChildren().add(flowPane);

                            compteur++;
                        }
                        //cadre.getChildren().add(test);
                        Scroll.setContent(test);


                    }

                }
            });
            listener = true;
        }
    }


    public static void setSynchro(Boolean s) {
        synchro = s;
    }

    public void chooseFile() {
        System.out.println("1");
        FileChooser fileChooser = new FileChooser();
        System.out.println("2");
        fileChooser.setTitle("Open Resource File");
        System.out.println("3");
        File file = fileChooser.showOpenDialog(mainApp.getPrimaryStage());
        System.out.println("ça passe");
        List<String> lines = new ArrayList<>();
        System.out.println(file.getAbsolutePath());
        try {
            lines = Files.readAllLines(Paths.get(file.getAbsolutePath().toString()));
        } catch (IOException e) {
            //e.printStackTrace();
            console.setText(console.getText() + e + "\n");
        }
        StringBuilder sb = new StringBuilder("%%");
        for (String s : lines)
            sb.append(s);
        sb.append("%%");

        try {
            InstrParser.parse(sb.toString());
            send(sb.toString());
        } catch (InvalidCommandException e) {
            //e.printStackTrace();
            console.setText(console.getText() + e + "\n");
        } catch (VariableException e) {
            //e.printStackTrace();
            console.setText(console.getText() + e + "\n");
        }

    }
    //bass x 45 1.1

}
