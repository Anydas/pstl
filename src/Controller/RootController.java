package Controller;

import javafx.fxml.FXML;
import javafx.scene.control.MenuBar;

import java.io.FileNotFoundException;

/**
 * Created by Valentin on 19/02/2017.
 */
public class RootController {

    @FXML
    private MenuBar bar;

    private Main mainApp;

    /**
     * The constructor.
     * The constructor is called before the initialize() method.
     */
    public RootController() {}
    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {}

    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setMainApp(Main mainApp) {
        this.mainApp = mainApp;
    }

}
